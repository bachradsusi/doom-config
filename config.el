;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(load (concat doom-private-dir "private.el"))

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
       doom-variable-pitch-font (font-spec :family "sans" :size 13))

(setq doom-font (font-spec :family "Source Code Pro"
                               :size 13
                               :weight 'semibold
                               :width 'normal)
      doom-big-font (font-spec :family "Source Code Pro"
                               :size 15
                               :weight 'semibold
                               :width 'normal)
      doom-variable-pitch-font (font-spec :family "Source Code Pro"
                               :size 14
                               :weight 'bold
                               :width 'normal))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
;; (setq doom-theme 'doom-tomorrow-day)
(setq doom-theme 'doom-acario-light)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; (setq org-directory "~/org/")
(setq org-directory "~/Documents/task")
(setq org-brain-path
      "~/Documents/brain")
(setq org-brain-visualize-default-choices 'all)
(setq org-brain-include-file-entries t)

(after! org
  (setq org-agenda-files '("~/Documents/task"))
  (setq org-todo-keywords
      '((sequence "BACKLOG" "TODO(t!)" "IN-PROGRESS(p!)" "WAITING" "|" "DONE" "DROPPED"))))

(setq org-agenda-sorting-strategy '(
                                    (agenda habit-down time-up priority-down)
                                    (todo deadline-down todo-state-down priority-down)
                                    (tags priority-down category-keep)
                                    (search category-keep)
                                    )
      )

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; (setq doom-leader-alt-key "M-m"
;;     doom-localleader-alt-key "M-m m")

;; https://emacs.stackexchange.com/questions/3458/how-to-switch-between-windows-quickly
(windmove-default-keybindings)
(windmove-default-keybindings 'control)

(map! "M-1" #'winum-select-window-1
      "M-2" #'winum-select-window-2
      "M-3" #'winum-select-window-3
      "M-4" #'winum-select-window-4
      "M-5" #'winum-select-window-5
      "M-6" #'winum-select-window-6
      "M-7" #'winum-select-window-7
      "C-<tab>" #'mode-line-other-buffer
      "<f12>" #'god-local-mode
      "C-<f12>" #'god-local-mode
      )

(setq projectile-project-search-path '("~/devel/fedora"))

(setq eshell-visual-commands '("ncmpcpp" "nvim" "vim" "vi" "screen" "tmux" "top" "htop" "less" "more" "lynx" "links" "ncftp" "mutt" "pine" "tin" "trn" "elm" "ssh" "bash" "zsh" "mutt"))
(setq eshell-visual-subcommands '(
                                  ("git" "diff" "log" "show")
                                  ))

;; https://github.com/syl20bnr/spacemacs/tree/develop/layers/%2Bemacs/org#org-journal
(setq org-journal-dir "~/Documents/journal/")
(setq org-journal-file-format "%Y-%m-%d")
(setq org-journal-date-prefix "#+TITLE: ")
(setq org-journal-date-format "%A, %B %d %Y")

(setq flyspell-default-dictionary "en")
